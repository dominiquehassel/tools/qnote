= qnode

:Author:    Dominique Hassel
:Email:     d-hassel@gmx.net

*qnode* is a cli to quickly create and manage nodes in a simple way.

A node is described by a title and has a larger description text. This text can be decoded in the _asciidoc_ format. You can define a date till when the node has to be done. 

== User Stories

 1) As a user I want to create a new node with a tile, a description and a target date.
 2) As a user I want to list all of my nodes.
 3) As a user I want to filter all nodes within a dedicated date.
 4) As a user I want to edit an existing node by chanigng the description and/or the date.
 5) As a user I want to delete an existing node.
 6) As a user I want to set a node to done. This won`t delete the node.
 7) As a user I want to filter the listed nodes with done/not done.


