package action

import (
	"github.com/google/uuid"
	"gitlab.com/dominiquehassel/tools/qnote/model"
	"gopkg.in/go-playground/validator.v9"
)


// CreateNewNode will create a new node and returns the node with a unique id.
// Will return an error when called with illegal values (nil values e.g.)
func CreateNewNode(title string, description string, date string)(node *model.QNode, err error) {
  uniqueID := uuid.New()

  qnode := &model.QNode{ ID: uniqueID, Title:title, Description:description}

  v := validator.New()
  
  e := v.Struct(qnode) 

  if e != nil {
    return nil, e
  }
  return qnode, nil
}