module gitlab.com/dominiquehassel/tools/qnote

go 1.14

require (
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/google/uuid v1.1.2
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	gopkg.in/go-playground/validator.v9 v9.31.0
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
