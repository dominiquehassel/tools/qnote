package model

import "github.com/google/uuid"

// QNode defines the simple node type with a title, a description and a date
type QNode struct {

	ID uuid.UUID `json:"id" validate:"required"`
	Title string `json:"title" validate:"required"`
	Description string `json:"description" validate:"required"`
}