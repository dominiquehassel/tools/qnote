/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/dominiquehassel/tools/qnote/action"
)

var title, description, date string

// newCmd represents the new command
var newCmd = &cobra.Command{
	Use:   "new",
	Short: "Creates a new node",
	Long: `Creates a new node. Each node must contain a title and a description and may have a destination date.
Each noe will be created with a unique id.`,
	Run: func(cmd *cobra.Command, args []string) {
		qnode, err := action.CreateNewNode(title,description,date)
		if err != nil {
			fmt.Errorf(err.Error())
		} else {
			fmt.Printf("Created new node with id: %s\n", qnode.ID)
		}
	},
}

func init() {
	rootCmd.AddCommand(newCmd)

	

	newCmd.Flags().StringVarP(&title,"title","t","","The title of the new node")
	newCmd.Flags().StringVarP(&description,"description","d","","The description text of the new node")
	newCmd.Flags().StringVarP(&date,"date","D","","The destination date of the new node. Must be defined in the format dd-mm-yyyy.")
}